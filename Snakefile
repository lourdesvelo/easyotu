rule all:
    input:
        expand("{sample}.phyloFlash.NTUfull_abundance.csv",
               sample=config["SAMPLES"]),
        "merge.rds"

rule QC:
    input:
        r1 = "{sample}_1.fastq.gz",
        r2 = "{sample}_2.fastq.gz"
    output:
        o1 = "{sample}_QC1.fastq.gz",
        o2 = "{sample}_QC2.fastq.gz"
    params:
        adapters = config["ADAPTER"],
    shell:
        "bbduk.sh in={input.r1} in2={input.r2} ref={params.adapters} out={output.o1} out2={output.o2} qtrim=rl trimq=20 maq=20 minlen=100"

rule kraken:
    input:
        r1 = "{sample}_QC1.fastq.gz",
        r2 = "{sample}_QC2.fastq.gz",
    output:
        k1 = "{sample}_1.fq",
        k2 = "{sample}_2.fq",
        report = "{sample}.krak"
    params:
        ref = config["HUMAN"],
    threads:
        20
    shell:
        "kraken2 --db {params.ref} --threads {threads} --paired  --gzip-compressed {input.r1} {input.r2} --output '-'  --report {output.report}  --unclassified-out {wildcards.sample}#.fq 2>&1 | sed 's/^/[kraken2] /' | tee -a {log}"

rule fq_gz:
    input:
        r1 = "{sample}_1.fq",
        r2 = "{sample}_2.fq"
    output:
        o1 = "{sample}_1.fq.gz",
        o2 = "{sample}_2.fq.gz"
    shell:
        "gzip {input.r1} && gzip {input.r2}"


def file_1(wildcards):
    if (config["HUMAN_FILTER"] is True):
        return expand("{sample}_1.fq.gz", sample=wildcards.sample)
    else:
        return expand("{sample}_QC1.fastq.gz", sample=wildcards.sample)


def file_2(wildcards):
    if (config["HUMAN_FILTER"] is True):
        return expand("{sample}_2.fq.gz", sample=wildcards.sample)
    else:
        return expand("{sample}_QC2.fastq.gz", sample=wildcards.sample)


rule phyloFlash:
    input:
        r1 = file_1,
        r2 = file_2
    output:
        "{sample}.phyloFlash.tar.gz"
    params:
        db = config["phyloFLASH"]
    conda:
        "envs/phyloflash.yaml"
    threads:
        10
    shell:
        "phyloFlash.pl -dbhome {params.db} -lib {wildcards.sample} -everything -readlength 150 -read1 {input.r1} -read2 {input.r2} -CPUs {threads}"

rule extract_NTU:
    input:
        "{sample}.phyloFlash.tar.gz",
    output:
        csv = "{sample}.phyloFlash.NTUfull_abundance.csv",
    shell:
        "tar -zxvf {input} {output.csv}"

rule NTU:
    input:
        expand("{sample}.phyloFlash.NTUfull_abundance.csv",
               sample=config["SAMPLES"])
    output:
        "merge.rds"
    conda:
        "envs/rscript.yaml"
    shell:
        "Rscript --vanilla ../scripts/flash2seq.R -o merge"
onsuccess:
    print(":) HAPPY")
