<table class="noBorder" align="center"><tr><td align="center" width="9999">
<img src="/images/logo.png" align="center" width="350" alt="Project icon">
</td></tr></table>

easyOTUs is a easy-to-use metagenomic pipeline based on snakemake. easyOTU is an efficient method for retrieving and analyzing SSU rRNA gene fragments from Illumina metagenomic sequences. It can be used for the rapid screening and comparison of multiple samples and can handle large whole-genome data. Since SSU rRNA-based community analysis is an important method in microbial ecology, this method can save projects with existing shotgun sequence data such as human sequencing projects from the additional cost of SSU rRNA sequencing.
EasyOTU takes fastq.gz (pair-end) as inputs and returns graphs/differents metrics, a phyloseq object and a biom file.

<table align="center"><tr><td align="center" width="9999">
<img src="/images/pipeline.png" align="center" width="500" alt="PIPELINE icon">
</td></tr></table>

## What does easyOTU do?
easyOTU is a flexible snakemake pipeline that carries out a quality check of your reads, remove human signal and reconstruct the SSU rRNAs from Illumina metagenomic experiments. It handles all this information to create a [phyloseq](https://joey711.github.io/phyloseq/) object in R and a biom file ready to be used in [qiime2](https://qiime2.org/). It will also produce graphs to start exploring the taxonomical composition of your data.

## Can I use easyOTU in a personal computer? 
easyOTU is based on snakemake which allows to run steps of the workflow in parallel on a cluster.  For real metagenomic data easyOTU should be run on a linux sytem, with enough memory (min ~50GB). It is set to use all cores for nnduk and 20 cores for kraken2 and phyloFlash. 

## How long I'll have to wait to have my OTU table with easyOTU?
easyOTU is so far the fastest combination of sofware we have tested. However, easyOTU speed will depend on your sample libraries. easyOTU will run for 10 minutes for a set of small libraries (around 100 000 reads paired-end each) but will take a day or more for a set of large libraries (around 10 000 000 - 50 000 000 reads paired end each).

## Can I set easyOTU to work with single-end fastq files?
It can be done... but we will need to twick a little bit our Snakemake rules. If you are interested in this, send us an email, we will do our best.

## Download from Gitlab
```bash
# Download from gitlab
git clone https://gitlab.com/lourdesvelo/easyotu.git
ls easyOTU
```
## Install prerequisites
easyOTU relies on the following software:
- [BBmap](https://github.com/BioInfoTools/BBMap)
- [kraken2](http://ccb.jhu.edu/software/kraken2/)
- [phyloFlash.pl](https://github.com/HRGV/phyloFlash)
- [Snakemake](snakemake)
- [R with their following libraries ](https://cran.r-project.org/src/base-prerelease/)
     - tidyverse
     - ggplot2
     - magrittr
     - purrr
     - phyloseq
     - optparse
     - scales
     - hrbrthemes

These tools need to be in your $PATH environment variable, so that easyOTU can find them. You can install all these dependencies using conda. Considering checking out the conda [user guide page](https://conda.io/projects/conda/en/latest/user-guide/index.html) if you are unfamiliar with conda 🙂. Some programs used by easyOTU are not compatible between each other, snakemake will deal with that using idenpendent conda evironments for some rules.
```bash
### This will create a new environment called easyOTU with all dependencies needed
conda env create -f easyOTU.yaml ## say yes to everything
conda activate easyOTU
```
## Setting up your references databases
easyOTU runs kraken2 (if human removal is needed, so optional) and phyloFlash to get to its final merge files. These two programs use reference files to do the job and you'll need to install and configure them before running easyOTU.

### Installing the kraken2 human database
To built a database in kraken2 we activate our easyOTU environment
```bash
conda activate easyOTU
```
- Install a taxonomy (provided by NCBI)
```bash
KRAKEN_DB="path to where you want your library to be installed"
kraken2-build --download-taxonomy --db $KRAKEN_DB
```
- Install the human reference library (GRCh38 human genome/proteins)
``` bash
kraken2-build --download-library human --no-masking --db $KRAKEN_DB
```
- Once your taxonomy and your library have been downloaded we will construct our library. You can use the --threads option to reduce build time. However, it will be long, you can strech your legs, have a coffee... whatever suits you... staring at the screen won't speed the process (we have already tried and it does not work, really).
```bash
kraken2-build --build --db $KRAKEN_DB
```
For more info go the kraken2 manual at [github](https://github.com/DerrickWood/kraken2/wiki/Manual#installation)

### Installing the latest SILVA database to be used by phyloFlash
phyloFlash is the heart of this pipeline and we are really greatfull to their developpers. You can check the phyloFlash paper [here](https://msystems.asm.org/content/5/5/e00920-20). phyloFalsh uses modified versions of the SILVA SSU database of small-subunit ribosomal RNA sequences. These files are maintained by the [ARB SILVA project](https://www.arb-silva.de/).

> NOTE: The SILVA license prohibits usage of the SILVA databases or parts of them within a non-academic/commercial environment beyond a 48h test period. If you want to use the SILVA databases with phyloFlash in a non-academic/commercial environment please contact them at contact(at)arb-silva.de.

Download the latest version of silva database from the [silva website](https://www.arb-silva.de/no_cache/download/archive/current/Exports/). The filename should be SILVA_XXX_SSURef_Nr99_tax_silva_trunc.fasta.gz where XXX is the current version number. Also download the UniVec database from [NCBI](https://www.ncbi.nlm.nih.gov/tools/vecscreen/univec/).
```bash
# to download the latest version of silva
wget https://www.arb-silva.de/fileadmin/silva_databases/current/Exports/SILVA_138.1_SSURef_NR99_tax_silva_trunc.fasta.gz
wget https://ftp.ncbi.nlm.nih.gov/pub/UniVec/UniVec_Core
```
phyloFlash is not compatible with our easyOTU environment (snakemake will deal with that when running the pipeline) but we will have to create an conda environment to set up our database:
```bash
conda env create -f envs/phyloflash.yaml
conda activate phyloFlash_otu
```
```bash
## this will construct a database in our working folder (we will use this path later on our config.yaml file for Snakemake)
phyloFlash_makedb.pl --univec_file /path/to/Univec --silva_file /path/to/SILVA_128_SSURef_Nr99_tax_silva_trunc.fasta.gz
```
Our phyloFlash conda environment is not longer needed so we can delete it.
```bash
conda deactivate phyloFlash_otu
conda env remove --name phyloFlash_otu
```

## Configure your analysis
### Create a config.yaml
Snakemake will need a config.yaml file to know how your samples are called and where your databases are. Your config.yaml file should look like this:
```yaml
ADAPTER: path/to/adapter.fa # included in the easyOTU git folder
HUMAN: $KRAKEN_DB #write the absolute path, not the variable
phyloFLASH: path/to/phyloflashdata/base
HUMAN_FILTER: True
SAMPLES:
 - SAMPLE1
 - SAMPLE2
```
### Create your data folder

Your work folder must contains the following files
```bash
├── Snakemake
├── config.yaml # (with your information, not the exemple ;)
├── data
│   ├── SAMPLE1_1.fastq.gz
│   ├── SAMPLE1_2.fastq.gz
│   ├── SAMPLE2_1.fastq.gz
│   ├── SAMPLE2_2.fastq.gz
├── envs
│   ├── phyloflash.yaml
│   └── rscript.yaml
├── scripts
│   ├── flash2seq.R
└── adapter.fa
```

## Run the pipeline / toy example
### Dry run 
You can test your dependencies with a snakemake dryrun and the toy exemple with have included in the toy directory. The toy folder contains 2 small libraries from human-microbiome samples. You can rapidly check if you're golden to go with your own analysis. Our samples are called A and B (we could have called then rusty and dusty but we wanted to make it simple).

```bash
conda activate easyOTU
snakemake --use-conda -npF -d toy  --configfile config.yaml
```

>CODE BREAKDOWN <br>
    snakemake  -our base command <br>
    --use-conda use conda environments included in the snakefile rules <br>
    -n perform a dry-run <br>
    -p print out the resulting shell command for illustration <br>
    -F force the execution of all rules <br>
    -d toy indicate your working directory where your fastq.gz files are <br>
    --configfile config.yaml use this config file <br> 

You can have a look to the pipeline with a [DAG](https://en.wikipedia.org/wiki/Directed_acyclic_graph) (directed acyclic graph) of all jobs. 
```
snakemake --use-conda -d toy --dag --configfile config.yaml | dot -Tsvg > dag.svg
```
This code will create a visualization of the DAG created with our toy data samples  using the dot command. We will create a svg image showing all the pipeline that will be carried out for our samples A and B. It will look like this:

![alt text](images/dag.svg "DAG pipeline")

### Go for the real thing
No errors?? You are golden, lets run the whole pipeline for A and B. <br>
This code will launch the whole pipeline. 
```bash
snakemake --use-conda --configfile config.yaml -d toy  --configfile config.yaml
```
## Outputs
easyOTU will output a lot of files in your data/toy folder:
- Compressed quality checked paired-end fastq.gz files (labelled QC#.fastq.gz).
- If remove host is selected :
    - Compressed non human paired-end fastq.gz files from kraken2 (labelled #.fq.gz).
    - Kraken2 report with the amount of human reads detected (SAMPLE#.krak).
- All [phyloFlash output](https://hrgv.github.io/phyloFlash/output.html).
- A phyloflash object called merge.rds ready to import to [R](https://www.r-project.org/).
- A biom file called merge_features-table.biom and a tax file called merge_tax.txt ready to import to [qiime2](https://qiime2.org/).
- Two png graphs showing the absolute (merge_absolute.png) and relative (merge_relative.png) abundance for your data using taxonomic kingdom and phylum information.

Our toy directory will look like this:
```
.
├── A_1.fq.gz
├── A_2.fq.gz
├── A.krak
├── A.phyloFlash.html
├── A.phyloFlash.log
├── A.phyloFlash.NTUfull_abundance.csv
├── A.phyloFlash.tar.gz
├── A_QC1.fastq.gz
├── A_QC2.fastq.gz
├── B_1.fq.gz
├── B_2.fq.gz
├── B.krak
├── B.phyloFlash.html
├── B.phyloFlash.log
├── B.phyloFlash.NTUfull_abundance.csv
├── B.phyloFlash.tar.gz
├── B_QC1.fastq.gz
├── B_QC2.fastq.gz
├── merge_absolute.png
├── merge_features-table.biom
├── merge.rds
├── merge_relative.png
└── merge_tax.txt
```
## Cite us! 
You can see easyOTU in action @ [Herzig et al., 2020](https://www.biorxiv.org/content/10.1101/2020.11.16.384438v1)

![alt text](/images/bioarch.png "QR")

## Acknowledgements
We thank colleagues and easyOTU users who have contributed to easyOTU development.

## Contact
Please feel free to fork and contribute! Pull request with suggested fixes will be always welcome.
